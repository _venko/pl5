use std::fmt;
use std::fmt::Display;
use std::ops::{Index, IndexMut};

/// A VM-native machine instruction.
type VmOpcode = usize;

/// A pointer to a cell in the VM dictionary's address space.
type VmPtr = usize;

/// A VM-native machine word. Interpreted as an isize since we want our Forth
/// to support negative numbers.
type VmWord = isize;

/// A single character. In this context, a "character" is whatever a u8 inside
/// a UTF-8 string is.
type VmCharacter = u8;

/// A wrapper around a VM-native opcode.
/// Used for combining the different interpretations of a memory cell into a
/// single type (below).
struct NativeInstr(VmOpcode);

/// A wrapper around a pointer in the VM dictionary's address space.
/// Used for combining the different interpretations of a memory cell into a
/// single type (below).
struct ForthInstr(VmPtr);

/// A wrapper around a VM-machine-word-sized literal integer.
/// Used for combining the different interpretations of a memory cell into a
/// single type (below).
struct MemoryInt(VmWord);

/// A wrapper around a character.
/// Used for combining the different interpretations of a memory cell into a
/// single type (below).
struct MemoryChar(VmCharacter);

/// A single cell of memory inside the VM's dictionary.
enum ForthCell {
    /// An operation in VM-native assembly.
    Opcode(NativeInstr),

    /// An operation as a pointer to a Forth word in the VM's dictionary.
    Word(ForthInstr),

    /// A piece of memory interpreted as an isize.
    IntCell(MemoryInt),

    /// A piece of memory representing a single character.
    CharCell(MemoryChar),
}

type DictIdx = usize;

/// The dictionary which holds all the words known by the Forth VM.
///
/// A word in the VM is comprised of a name, a link pointer, a set of tags, a
/// code segment, and a data segment.
/// * The name of the dictionary is the string name the user or implementor chose
/// for the word, ie what is recognized by the interpreter from the user's text
/// input.
/// * The link pointer is an address to the dictionary word defined immediatly
/// prior to the word under consideration. It points at the start of the prior
/// word (the first character of its name).
/// * The tags for a word are used to indicate information about the word such
/// as whether it is "immediate" or "hidden".
/// * The code segment is the region of memory that defines how the word affects
/// the state of the machine. This can include affects to the machine's stack as
/// well as any of the machine's metadata like its mode field, error status,
/// etc.
/// * The data segment is the region of memory reserved by the word and acts
/// similar to a heap. For words declared as simple VARIABLEs and CONSTANTs, this
/// region is a single cell, but the user can expand the size of this region to
/// any capacity they wish using more sophisticated declaration words. This
/// allows the programmer to define composite data structures such as arrays and
/// records, though because the data segment of the word is inherently
/// unstructured it is up to the programmer to also define words that correctly
/// interpret the memory in that segment according to the semantics of their
/// structure.
struct Dictionary(Vec<ForthCell>);

impl Index<DictIdx> for Dictionary {
    type Output = ForthCell;
    fn index(&self, idx: DictIdx) -> &Self::Output {
        &self.0[idx]
    }
}

impl IndexMut<DictIdx> for Dictionary {
    fn index_mut(&mut self, idx: DictIdx) -> &mut Self::Output {
        &mut self.0[idx]
    }
}

/// The stack which holds the working memory of the Forth VM.
#[derive(Clone, Debug)]
struct Stack(Vec<VmWord>);

// NOTE: This trait is implemented to support potential low-level operations
// but in general it is best to avoid using it when one of the types functions
// can achieve the same thing.
impl<T> Index<T> for Stack
where
    T: Into<usize>,
{
    type Output = VmWord;
    fn index(&self, idx: T) -> &Self::Output {
        &self.0[idx.into()]
    }
}

// NOTE: This trait is implemented to support potential low-level operations
// but in general it is best to avoid using it when one of the types functions
// can achieve the same thing.
impl<T> IndexMut<T> for Stack
where
    T: Into<usize>,
{
    fn index_mut(&mut self, idx: T) -> &mut Self::Output {
        &mut self.0[idx.into()]
    }
}

impl Display for Stack {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<{}>", self.0.len())?;
        for elem in self.0.iter() {
            write!(f, " {}", elem)?;
        }
        Ok(())
    }
}

impl Stack {
    /// Creates a new instance of a stack.
    pub fn new() -> Stack {
        Stack(vec![])
    }

    /// Returns the length of the given stack.
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Returns whether the given stack is empty.
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    /// Clears the given stack of all its elements.
    ///
    /// Does not affect the total capacity of the stack.
    pub fn clear(&mut self) {
        self.0.clear();
    }

    /// Pushes a single value onto the top of the given stack.
    pub fn push(&mut self, word: VmWord) {
        self.0.push(word);
    }

    /// Pops a single value off the top of the stack and returns it, if the
    /// stack is non-empty.
    pub fn pop(&mut self) -> Option<VmWord> {
        self.0.pop()
    }

    /// Returns the value of the top of the stack. Equivalent to a "peek" but
    /// named top since peeks conventionally do not support mutations like
    /// `top_mut` below.
    pub fn top(&self) -> Option<&VmWord> {
        self.0.last()
    }

    /// Returns a mutable reference to the top of the given stack.
    ///
    /// Intended for use as an optimization over popping a value from the stack
    /// and immediately pushing another.
    pub fn top_mut(&mut self) -> Option<&mut VmWord> {
        self.0.last_mut()
    }

    /// Returns a slice of the top N elements of the given stack.
    ///
    /// Elements are ordered in the same way the stack is are represented when
    /// printed, ie the leftmost (0th) element is the deepest stack element and
    /// the rightmost (Nth) element is the top of the stack.
    pub fn top_n(&self, n: usize) -> Option<&[VmWord]> {
        let len = self.len();
        if len < n {
            None
        } else {
            let hi = len - 1;
            let lo = hi - n;
            Some(&self.0[lo..hi])
        }
    }

    /// Returns a mutable slice of the top N elements of the given stack.
    ///
    /// Elements are ordered in the same way the stack is are represented when
    /// printed, ie the leftmost (0th) element is the deepest stack element and
    /// the rightmost (Nth) element is the top of the stack.
    ///
    /// Intended for use as an optimization over popping multiple values from
    /// and to the stack for operations that do not affect the total number
    /// of stack elements.
    pub fn top_n_mut(&mut self, n: usize) -> Option<&mut [VmWord]> {
        let len = self.len();
        if len < n {
            None
        } else {
            let hi = len - 1;
            let lo = hi - n;
            Some(&mut self.0[lo..hi])
        }
    }
}

/// A region of memory held by the machine that holds the most recent user
/// input.
///
/// Storing the user input in a region under the control of the Forth VM allows
/// the VM to implement things like consumption, tokenization, and
/// interpretation of user input at the language level (ie as Forth words),
/// rather than relying on "intrinsic" operations inaccessible to the programmer.
// TODO: Decide on a represntation for this and implement it.
struct InputBuffer;

struct Vm {
    dict: Dictionary,
    stack: Stack,
    input: InputBuffer,
}

fn main() {
    println!("Hello, world!");
}
